var express = require('express');
var router = express.Router();

const bcrypt = require("bcrypt");
const jwt = require('jsonwebtoken');
const dotenv = require('dotenv');

require("dotenv").config();

dotenv.config();

const Sequelize = require('sequelize');
const respuestas = require('../models').Respuestas;
const usuarios = require('../models').Usuarios;
// const usuarios = require('../models').Usuarios;



const path = require('path');



const EstadisticasController = require('../Controllers/EstadisticasController');
const EstudiosController = require('../Controllers/EstudiosController');
const PreguntasController = require('../Controllers/PreguntasController');
const RespuestasController = require('../Controllers/RespuestasController');
const UsuariosController = require('../Controllers/UsuariosController');
const RegistroMultimediaController = require('../Controllers/RegistroMultimediaController');


/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/api/estadisticas/',EstadisticasController.List);
router.get('/api/estadisticas/id/:id',EstadisticasController.ListAt);
router.delete('/api/estadisticas/id/:id',EstadisticasController.DeleteEstadisticas);
router.patch('/api/estadisticas/id/:id',EstadisticasController.UpdateEstadisticas);
router.post('/api/estadisticas/:enviadas/:respondidas/:puntuacion/:id_usuarios',EstadisticasController.CreateEstadisticas);

router.get('/api/estudios',EstudiosController.List);
router.get('/api/estudiosid/:id',EstudiosController.ListAt);
router.delete('/api/estudiosid/:id',EstudiosController.DeleteEstudios);
router.patch('/api/estudiosid/:id',EstudiosController.UpdateEstudios);
router.post('/api/estudios/:tipo_nivel_escolaridad/:nombre_titulo/:descripcion_experiencia/:id_usuarios',EstudiosController.CreateEstudios);

router.get('/api/preguntas/',PreguntasController.List);
router.get('/api/preguntas/id/:id',PreguntasController.ListAt);
router.delete('/api/preguntas/id/:id',PreguntasController.DeletePreguntas);
router.patch('/api/preguntas/id/:id',PreguntasController.UpdatePreguntas);
router.post('/api/preguntas/:titulo/:descripcion/:fecha_publicacion/:id_usuarios',PreguntasController.CreatePreguntas);

router.get('/api/respuestas/',RespuestasController.List);
router.get('/api/respuestas/id/:id',RespuestasController.ListAt);
router.delete('/api/respuestas/id/:id',RespuestasController.DeleteRespuestas);
router.patch('/api/respuestas/id/:id',RespuestasController.UpdateRespuestas);
router.post('/api/respuestas/:nombre_usuario/:cantidad_like/:cantidad_no_like/:fecha_respuestas/:contenido/:id_usuarios/:id_preguntas',RespuestasController.CreateRespuestas);

router.get('/api/usuarios/',UsuariosController.List);
router.get('/api/usuarios/id/:id',UsuariosController.ListAt);
router.delete('/api/usuarios/id/:id',UsuariosController.DeleteUsuarios);
router.patch('/api/usuarios/id/:id',UsuariosController.UpdateUsuarios);
router.post('/api/usuarios/:nombres/:apellidos/:fecha_nacimiento/:direccion/:genero/:telefono/:email/:password',UsuariosController.CreateUsuarios);

router.get('/api/registroMultimedia/',RegistroMultimediaController.List);
router.get('/api/registroMultimedia/id/:id',RegistroMultimediaController.ListAt);
router.delete('/api/registroMultimedia/id/:id',RegistroMultimediaController.DeleteRegistroMultimedia);
router.patch('/api/registroMultimedia/id/:id',RegistroMultimediaController.UpdateRegistroMultimedia);
router.post('/api/registroMultimedia/:nombre_archivo/:directorio_archivo/:id_preguntas',RegistroMultimediaController.CreateRegistroMultimedia);

// Consultas Kevin Rendon
router.get('/Usuarios/Respuestas',UsuariosController.ListarUsuariosRespuestas);
router.get('/Preguntas/Respuestas/RegistroMultimedia',PreguntasController.ListarPreguntasRespuestasRegistroMultimedia);

// Consultas Andres Muñoz
router.get('/Usuarios/Estudios',UsuariosController.ListarUsuariosEstudios);
router.get('/Usuarios/Estadisticas/Respuestas',UsuariosController.ListarUsuariosEstadisticasRespuestas);

// consultas Elkin Valencia
router.get('/Usuarios/Preguntas',UsuariosController.ListarUsuariosPreguntas);
router.get('/Usuarios/Respuestas/Preguntas',UsuariosController.ListarUsuariosRespuestasPreguntas);

// Consultas Jeferson Guetio
router.get('/Usuarios/Estadisticas',UsuariosController.ListarUsuariosEstadisticas);
router.get('/Usuarios/Estudios/Estadisticas',UsuariosController.ListarUsuariosEstudiosEstadisticas);

// Consultas Lisbeth Sanchez
router.get('/Preguntas/Respuestas',PreguntasController.ListarPreguntasRespuestas);
router.get('/Preguntas/RegistroMultimedia',PreguntasController.ListarPreguntasRegistroMultimedia);

// Consultas Jose Hernandez 
// router.get('/Usuarios/Estudios',UsuariosController.ListarUsuariosEstudios);
// router.get('/Usuarios/Respuestas',UsuariosController.ListarUsuariosRespuestas);

router.post('/api/losgin',UsuariosController.loginUsuario);

// Registro y Login de usuario

 router.post('/', async(req, res, next)=>{
  //res.status(201).json(req.body);
  //add new user and return 201
  const salt = await bcrypt.genSalt(10);
  var usr = {
    nombres: req.body.nombres,
          apellidos: req.body.apellidos,
          fecha_nacimiento: req.body.fecha_nacimiento,
          direccion: req.body.direccion,
          genero: req.body.genero,
          telefono: req.body.telefono,
          email: req.body.email,
    password: await bcrypt.hash(req.body.password, salt)
  };
  created_user = await usuarios.create(usr);
  res.status(201).json(created_user);
});

router.post('/login',async(req,res,next)=>{
  const user = await usuarios.findOne({ where : {email : req.body.email}});
  if(user){
     const password_valid = await bcrypt.compare(req.body.password, user.password);
     if(password_valid){
         token = jwt.sign({ "id" : user.id,"email" : user.email,"nombres":user.nombres },process.env.JWT_KEY);
         res.status(200).json({ token : token });
     } else {
       res.status(400).json({ error : "Contraseña incorrecta" });
     }
   
   }else{
     res.status(404).json({ error : "El usuario no existe" });
   }
   
   });

   router.get('/autorizacion',
   async(req,res,next)=>{
    try {
      let token = req.headers['authorization'].split(" ")[1];
      let decoded = jwt.verify(token,process.env.JWT_KEY);
      req.user = decoded;
      next();
    } catch(err){
      res.status(401).json({"msg":"No se pudo autenticar"});
    }
    },
    async(req,res,next)=>{
      let user = await usuarios.findOne({where:{id : req.user.id},attributes:{exclude:["password"]}});
      if(user === null){
        res.status(404).json({'msg':"Usuario no encontrado"});
      }
      res.status(200).json(user);
      
    });

module.exports = router;
