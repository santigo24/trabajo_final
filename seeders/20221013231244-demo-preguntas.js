"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    return queryInterface.bulkInsert("preguntas", [
      {
        titulo: "Sembrado de maiz",
        descripcion: "¿Cómo es el proceso de siembra del maíz?",
        fecha_publicacion: "2022-10-13",
        id_usuarios: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  async down(queryInterface, Sequelize) {
    return queryInterface.bulkDelete("preguntas", null, {});
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
