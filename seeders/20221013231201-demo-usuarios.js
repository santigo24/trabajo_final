"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    return queryInterface.bulkInsert("usuarios", [
      {
        nombres: "kevin",
        apellidos: "rendon",
        fecha_nacimiento: "2000-01-24",
        direccion: "Manzana 15 #15-42",
        genero: "Masculino",
        telefono: "2338882",
        email: "santigo24@gmail.com",
        password: "1233445",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  async down(queryInterface, Sequelize) {
    return queryInterface.bulkDelete("usuarios", null, {});
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
