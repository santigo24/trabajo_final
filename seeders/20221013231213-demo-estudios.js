"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    return queryInterface.bulkInsert("estudios", [
      {
        tipo_nivel_escolaridad: "Secundaria",
        nombre_titulo: "Tecnica secundaria",
        descripcion_experiencia: "Trabajo social",
        id_usuarios: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  async down(queryInterface, Sequelize) {
    return queryInterface.bulkDelete("estudios", null, {});
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
