"use strict";
const { Model } = require("sequelize");
const Preguntas = require("./preguntas");
module.exports = (sequelize, DataTypes) => {
  class RegistroMultimedia extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  RegistroMultimedia.init(
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      nombre_archivo: {
        type: DataTypes.STRING(100),
        defaultValue: false,
        allowNull: false,
      },
      directorio_archivo: {
        type: DataTypes.STRING(100),
        defaultValue: false,
        allowNull: false,
      },
      id_preguntas: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: { model: "preguntas", key: "id" },
      },
    },
    {
      sequelize,
      modelName: "RegistroMultimedia",
      tableName: "registroMultimedia",
    }
  );
  return RegistroMultimedia;
};
