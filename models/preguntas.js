"use strict";
const { Model } = require("sequelize");
const Respuestas = require("./respuestas");
const RegistroMultimedia = require("./registroMultimedia");
const Usuarios = require("./usuarios");
module.exports = (sequelize, DataTypes) => {
  class Preguntas extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Preguntas.hasMany(models.Respuestas, { foreignKey: "id_preguntas" });
      Preguntas.hasMany(models.RegistroMultimedia, { foreignKey: "id_preguntas" });
      Preguntas.belongsTo(models.Usuarios, {foreignKey: 'id_usuarios'});
    }
  }
  Preguntas.init(
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      titulo: {
        type: DataTypes.STRING(100),
        defaultValue: false,
        allowNull: false,
      },
      descripcion: {
        type: DataTypes.STRING(100),
        defaultValue: false,
        allowNull: false,
      },
      fecha_publicacion: {
        type: DataTypes.DATE,
      },
      id_usuarios: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: { model: "usuarios", key: "id" },
      },
    },
    {
      sequelize,
      modelName: "Preguntas",
      tableName: "preguntas",
    }
  );
  return Preguntas;
};
