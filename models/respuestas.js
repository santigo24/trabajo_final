"use strict";
const { Model } = require("sequelize");
const Usuarios = require("./usuarios");
const Preguntas = require("./preguntas");
module.exports = (sequelize, DataTypes) => {
  class Respuestas extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Respuestas.belongsTo(models.Usuarios, { foreignKey: "id_usuarios" });
    }
  }
  Respuestas.init(
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      nombre_usuario: {
        type: DataTypes.STRING(100),
        defaultValue: false,
        allowNull: false,
      },
      cantidad_like: {
        type: DataTypes.INTEGER(100),
        defaultValue: false,
        allowNull: false,
      },
      cantidad_no_like: {
        type: DataTypes.INTEGER(100),
        defaultValue: false,
        allowNull: false,
      },
      fecha_respuestas: {
        type: DataTypes.DATE,
        defaultValue: false,
        allowNull: false,
      },
      contenido: {
        type: DataTypes.STRING(200),
        defaultValue: false,
        allowNull: false,
      },
      id_usuarios: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: { model: "usuarios", key: "id" },
      },
      id_preguntas: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: { model: "preguntas", key: "id" },
      },
    },
    {
      sequelize,
      modelName: "Respuestas",
      tableName: "respuestas",
    }
  );
  return Respuestas;
};
