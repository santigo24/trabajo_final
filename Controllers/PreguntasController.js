const Sequelize = require("sequelize");
const preguntas = require("../models").Preguntas;
const respuestas = require("../models").Respuestas;
const registroMultimedia = require("../models").RegistroMultimedia;

module.exports = {
  List(_, res) {
    return preguntas
      .findAll({})
      .then((preguntas) => res.status(200).send(preguntas))
      .catch((error) => res.status(400).send(error));
  },
  ListAt(req, res) {
    return preguntas
      .findAll({
        where: {
          id: req.params.id,
        },
      })
      .then((preguntas) => res.status(200).send(preguntas))
      .catch((error) => res.status(400).send(error));
  },

  DeletePreguntas(req, res) {
    return preguntas
      .destroy({
        where: {
          id: req.params.id,
        },
      })
      .then((preguntas) => res.sendStatus(preguntas))
      .catch((error) => res.status(400).send(error));
  },

  UpdatePreguntas(req, res) {
    return preguntas
      .update(
        {
          titulo: req.body.titulo,
          descripcion: req.body.descripcion,
          fecha_publicacion: req.body.fecha_publicacion,
          id_usuarios: req.body.id_usuarios,
        },
        {
          where: {
            id: req.params.id,
          },
        }
      )
      .then((result) => {
        res.json(result);
      });
  },
  CreatePreguntas(req, res) {
    return preguntas
      .create({
        titulo: req.params.titulo,
        descripcion: req.params.descripcion,
        fecha_publicacion: req.params.fecha_publicacion,
        id_usuarios: req.params.id_usuarios,
      })
      .then((preguntas) => res.status(200).send(preguntas))
      .catch((error) => res.status(400).send(error));
  },
  // Consultas Kevin Rendon
  ListarPreguntasRespuestasRegistroMultimedia(req, res) {
    return preguntas
      .findAll({
        include: [
          { model: respuestas, require: true },
          { model: registroMultimedia, require: true },
        ],
      })
      .then((usuarios) => res.status(200).send(usuarios))
      .catch((error) => res.status(400).send(error));
  },
  // Consultas Lisbeth Sanchez
  ListarPreguntasRespuestas(req, res) {
    return preguntas
      .findAll({
        include: [{ model: respuestas, require: true }],
      })
      .then((usuarios) => res.status(200).send(usuarios))
      .catch((error) => res.status(400).send(error));
  },
  ListarPreguntasRegistroMultimedia(req, res) {
    return preguntas
      .findAll({
        include: [{ model: registroMultimedia, require: true }],
      })
      .then((usuarios) => res.status(200).send(usuarios))
      .catch((error) => res.status(400).send(error));
  },
};
