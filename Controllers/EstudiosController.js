const Sequelize = require("sequelize");
const estudios = require("../models").Estudios;

module.exports = {
  List(_, res) {
    return estudios
      .findAll({})
      .then((estudios) => res.status(200).send(estudios))
      .catch((error) => res.status(400).send(error));
  },
  ListAt(req, res) {
    return estudios
      .findAll({
        where: {
          id: req.params.id,
        },
      })
      .then((estudios) => res.status(200).send(estudios))
      .catch((error) => res.status(400).send(error));
  },

  DeleteEstudios(req, res) {
    return estudios
      .destroy({
        where: {
          id: req.params.id,
        },
      })
      .then((estudios) => res.sendStatus(estudios))
      .catch((error) => res.status(400).send(error));
  },

  UpdateEstudios(req, res) {
    return estudios
      .update(
        {
          tipo_nivel_escolaridad: req.body.tipo_nivel_escolaridad,
          nombre_titulo: req.body.nombre_titulo,
          descripcion_experiencia: req.body.descripcion_experiencia,
          id_usuarios: req.body.id_usuarios,
        },
        {
          where: {
            id: req.params.id,
          },
        }
      )
      .then((result) => {
        res.json(result);
      });
  },
  CreateEstudios(req, res) {
    return estudios
      .create({
        tipo_nivel_escolaridad: req.params.tipo_nivel_escolaridad,
        nombre_titulo: req.params.nombre_titulo,
        descripcion_experiencia: req.params.descripcion_experiencia,
        id_usuarios: req.params.id_usuarios,
      })
      .then((estudios) => res.status(200).send(estudios))
      .catch((error) => res.status(400).send(error));
  },
};
