const Sequelize = require("sequelize");
const registroMultimedia = require("../models").RegistroMultimedia;

module.exports = {
  List(_, res) {
    return registroMultimedia
      .findAll({})
      .then((registroMultimedia) => res.status(200).send(registroMultimedia))
      .catch((error) => res.status(400).send(error));
  },
  ListAt(req, res) {
    return registroMultimedia
      .findAll({
        where: {
          id: req.params.id,
        },
      })
      .then((registroMultimedia) => res.status(200).send(registroMultimedia))
      .catch((error) => res.status(400).send(error));
  },

  DeleteRegistroMultimedia(req, res) {
    return registroMultimedia
      .destroy({
        where: {
          id: req.params.id,
        },
      })
      .then((registroMultimedia) => res.sendStatus(registroMultimedia))
      .catch((error) => res.status(400).send(error));
  },

  UpdateRegistroMultimedia(req, res) {
    return registroMultimedia
      .update(
        {
          nombre_archivo: req.body.nombre_archivo,
          directorio_archivo: req.body.directorio_archivo,
          id_preguntas: req.body.id_preguntas,
        },
        {
          where: {
            id: req.params.id,
          },
        }
      )
      .then((result) => {
        res.json(result);
      });
  },
  CreateRegistroMultimedia(req, res) {
    return registroMultimedia
      .create({
        nombre_archivo: req.params.nombre_archivo,
        directorio_archivo: req.params.directorio_archivo,
        id_preguntas: req.params.id_preguntas,
      })
      .then((registroMultimedia) => res.status(200).send(registroMultimedia))
      .catch((error) => res.status(400).send(error));
  },
};
