const Sequelize = require("sequelize");

const usuarios = require("../models").Usuarios;
const respuestas = require("../models").Respuestas;
const estudios = require("../models").Estudios;
const estadisticas = require("../models").Estadisticas;
const preguntas = require("../models").Preguntas;

module.exports = {
  List(_, res) {
    return usuarios
      .findAll({})
      .then((usuarios) => res.status(200).send(usuarios))
      .catch((error) => res.status(400).send(error));
  },
  ListAt(req, res) {
    return usuarios
      .findAll({
        where: {
          id: req.params.id,
        },
      })
      .then((usuarios) => res.status(200).send(usuarios))
      .catch((error) => res.status(400).send(error));
  },

  DeleteUsuarios(req, res) {
    return usuarios
      .destroy({
        where: {
          id: req.params.id,
        },
      })
      .then((usuarios) => res.sendStatus(usuarios))
      .catch((error) => res.status(400).send(error));
  },

  UpdateUsuarios(req, res) {
    return usuarios
      .update(
        {
          nombres: req.body.nombres,
          apellidos: req.body.apellidos,
          fecha_nacimiento: req.body.fecha_nacimiento,
          direccion: req.body.direccion,
          genero: req.body.genero,
          telefono: req.body.telefono,
          email: req.body.email,
          password: req.body.password,
        },
        {
          where: {
            id: req.params.id,
          },
        }
      )
      .then((result) => {
        res.json(result);
      });
  },
  CreateUsuarios(req, res) {
    return usuarios
      .create({
        nombres: req.params.nombres,
        apellidos: req.params.apellidos,
        fecha_nacimiento: req.params.fecha_nacimiento,
        direccion: req.params.direccion,
        genero: req.params.genero,
        telefono: req.params.telefono,
        email: req.params.email,
        password: req.params.password,
      })
      .then((usuarios) => res.status(200).send(usuarios))
      .catch((error) => res.status(400).send(error));
  },
  // Consultas Kevin Rendon
  ListarUsuariosRespuestas(req, res) {
    return usuarios
      .findAll({
        include: [{ model: respuestas, require: true }],
      })
      .then((usuarios) => res.status(200).send(usuarios))
      .catch((error) => res.status(400).send(error));
  },
  // Consultas Andres Muñoz
  ListarUsuariosEstudios(req, res) {
    return usuarios
      .findAll({
        include: [{ model: estudios, require: true }],
      })
      .then((usuarios) => res.status(200).send(usuarios))
      .catch((error) => res.status(400).send(error));
  },
  ListarUsuariosEstadisticasRespuestas(req, res) {
    return usuarios
      .findAll({
        include: [
          { model: estadisticas, require: true },
          { model: respuestas, require: true },
        ],
      })
      .then((usuarios) => res.status(200).send(usuarios))
      .catch((error) => res.status(400).send(error));
  },
  // Consultas Elkin Valencia
  ListarUsuariosPreguntas(req, res) {
    return usuarios
      .findAll({
        include: [{ model: preguntas, require: true }],
      })
      .then((usuarios) => res.status(200).send(usuarios))
      .catch((error) => res.status(400).send(error));
  },
  ListarUsuariosRespuestasPreguntas(req, res) {
    return usuarios
      .findAll({
        include: [
          { model: respuestas, require: true },
          { model: preguntas, require: true },
        ],
      })
      .then((usuarios) => res.status(200).send(usuarios))
      .catch((error) => res.status(400).send(error));
  },
  // Jeferson Guetio
  ListarUsuariosEstadisticas(req, res) {
    return usuarios
      .findAll({
        include: [{ model: estadisticas, require: true }],
      })
      .then((usuarios) => res.status(200).send(usuarios))
      .catch((error) => res.status(400).send(error));
  },
  ListarUsuariosEstudiosEstadisticas(req, res) {
    return usuarios
      .findAll({
        include: [
          { model: estudios, require: true },
          { model: estadisticas, require: true },
        ],
      })
      .then((usuarios) => res.status(200).send(usuarios))
      .catch((error) => res.status(400).send(error));
  },
  loginUsuario(req, res){
    //const usuario = req.body.usuario;
    //const clave = req.body.clave;
    //recuperar los datos del usuario de la bd
    const user = usuarios.findOne({ where : {email : req.body.email, password : req.body.password }});
    
    if (req.body.email === user.email && req.body.password === user.password) {
      const datos = {
        id: user.id,
        nombres: user.nombres,
        email: user.email,
        //codigo: x.codigo,
      };
      const token = jwt.sign(
        { id: datos.id, email: datos.email },
        TOKEN_KEY,
        { expiresIn: "2h" }
      );
      let nDatos = { ...datos, token };
      res.status(200).send(json(nDatos));
    } else {
      res.status(400).send("Crendenciales incorrectas");
    }    


},
}
