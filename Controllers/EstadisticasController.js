const Sequelize = require("sequelize");
const estadisticas = require("../models").Estadisticas;

module.exports = {
  List(_, res) {
    return estadisticas
      .findAll({})
      .then((estadisticas) => res.status(200).send(estadisticas))
      .catch((error) => res.status(400).send(error));
  },
  ListAt(req, res) {
    return estadisticas
      .findAll({
        where: {
          id: req.params.id,
        },
      })
      .then((estadisticas) => res.status(200).send(estadisticas))
      .catch((error) => res.status(400).send(error));
  },

  DeleteEstadisticas(req, res) {
    return estadisticas
      .destroy({
        where: {
          id: req.params.id,
        },
      })
      .then((estadisticas) => res.sendStatus(estadisticas))
      .catch((error) => res.status(400).send(error));
  },

  UpdateEstadisticas(req, res) {
    return estadisticas
      .update(
        {
          enviadas: req.body.enviadas,
          respondidas: req.body.respondidas,
          puntuacion: req.body.puntuacion,
          id_usuarios: req.body.id_usuarios,
        },
        {
          where: {
            id: req.params.id,
          },
        }
      )
      .then((result) => {
        res.json(result);
      });
  },
  CreateEstadisticas(req, res) {
    return estadisticas
      .create({
        enviadas: req.params.enviadas,
        respondidas: req.params.respondidas,
        puntuacion: req.params.puntuacion,
        id_usuarios: req.params.id_usuarios,
      })
      .then((estadisticas) => res.status(200).send(estadisticas))
      .catch((error) => res.status(400).send(error));
  },
};
