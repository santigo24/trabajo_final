"use strict";
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("estadisticas", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      enviadas: {
        type: Sequelize.INTEGER(100),
        defaultValue: false,
        allowNull: false,
      },
      respondidas: {
        type: Sequelize.INTEGER(100),
        defaultValue: false,
        allowNull: false,
      },
      puntuacion: {
        type: Sequelize.INTEGER(100),
        defaultValue: false,
        allowNull: false,
      },
      id_usuarios: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: {
            tableName: "usuarios",
          },
          key: "id",
        },
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("estadisticas");
  },
};
