"use strict";
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("preguntas", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      titulo: {
        type: Sequelize.STRING(100),
        defaultValue: false,
        allowNull: false,
      },
      descripcion: {
        type: Sequelize.STRING(100),
        defaultValue: false,
        allowNull: false,
      },
      fecha_publicacion: {
        type: Sequelize.DATE,
      },
      id_usuarios: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: {
            tableName: "usuarios",
          },
          key: "id",
        },
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("preguntas");
  },
};
